<!DOCTYPE html>
<html>
<body>


<?php
   session_start(); 
   $input_email = $_POST['Email'];
   $input_nickname = $_POST['NickName'];
   $input_address= $_POST['Address'];
   $input_pwd = $_POST['Password']; 
   $input_phonenumber = $_POST['PhoneNumber']; 
   $input_id = $_SESSION['id'];
   $conn = getDB();

   $sql="";

   // safe profile editing : use of a prepared statement
   if($input_pwd!=''){
      $input_pwd = password_hash($input_pwd, PASSWORD_ARGON2I);
      if (!($stmt = $conn->prepare("UPDATE credential 
                                       SET nickname= (?),email= (?),
                                       address= (?),Password= (?),
                                       PhoneNumber= (?) where ID= (?);"))) {
         echo "Prepared statement failure : (" . $conn->errno . ") " . $conn->error;  
      }
      if (!$stmt->bind_param("sssssi", $input_nickname , $input_email , $input_address , 
                                       $input_pwd , $input_phonenumber , $input_id)) {
      echo "Parameter injections failure : (" . $conn->errno . ") " . $conn->error;
      }
   }else{
      if (!($stmt = $conn->prepare("UPDATE credential 
                                       SET nickname= (?),email= (?),
                                       address= (?),PhoneNumber= (?) 
                                       where ID= (?);"))) {
         echo "Prepared statement failure : (" . $conn->errno . ") " . $conn->error;  
      }
      if (!$stmt->bind_param("ssssi", $input_nickname , $input_email , $input_address ,
                                     $input_phonenumber , $input_id)) {
      echo "Parameter injections failure : (" . $conn->errno . ") " . $conn->error;
      }
   }

  if (!$stmt->execute()) {
    echo "Execution failure : (" . $conn->errno . ") " . $conn->error;
  } else {
    $result = $stmt->get_result();
  }
  $stmt->close();

   $conn->query($sql);
   $conn->close();	
   header("Location: unsafe_credential.php");
   exit();

function getDB() {
   $dbhost="localhost";
   $dbuser="updateapp";
   $dbpass="updateapp";
   $dbname="Users";


   // Create a DB connection
   $conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
   if ($conn->connect_error) {
         die("Connection failed: " . $conn->connect_error . "\n");
   }
return $conn;
}
 
?>

</body>
</html> 
