
DROP DATABASE IF EXISTS `Users`;
CREATE DATABASE `Users`;
USE Users;

--
-- Table structure for table `credential`
--

DROP TABLE IF EXISTS `credential`;

CREATE TABLE `credential` (
  `ID` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(30) NOT NULL,
  `EID` varchar(20) DEFAULT NULL,
  `Salary` int(9) DEFAULT NULL,
  `birth` varchar(20) DEFAULT NULL,
  `SSN` varchar(20) DEFAULT NULL,
  `PhoneNumber` varchar(20) DEFAULT NULL,
  `Address` varchar(300) DEFAULT NULL,
  `Email` varchar(300) DEFAULT NULL,
  `NickName` varchar(300) DEFAULT NULL,
  `Password` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ;


INSERT INTO `credential` VALUES 
(1,'Alice','10000',20000,'9/20','10211002','','','','','$argon2i$v=19$m=65536,t=4,p=1$SEY1RlYzbHFScjdqMHQ5Rw$DHM2569hy2l1MALdZQ03Vk6R4ZzAUO3oCcRHPIJN0oM'),
(2,'Boby','20000',30000,'4/20','10213352','','','','','$argon2i$v=19$m=65536,t=4,p=1$azZ5R3pWWno2Z2J4TnlzUA$cYMP2lWGsF6WZXzsyBlaRHFUJXRe6Te2W0zrGiEr7Zs'),
(3,'Ryan','30000',50000,'4/10','98993524','','','','','$argon2i$v=19$m=65536,t=4,p=1$Q2RYR1RxM1BiNnBmY1hleg$Sd75N0aHW7ol5V4SKTO3nh/AcQ1fOkrJbdlKcXzlDAY'),
(4,'Samy','40000',90000,'1/11','32193525','','','','','$argon2i$v=19$m=65536,t=4,p=1$RllzcVp0VE1rRUpXeUpJdw$0ynpC6k5C0FFZ+9O0kjNyDDxqOobrBPERLZG6hp89C0'),
(5,'Ted','50000',110000,'11/3','32111111','','','','','$argon2i$v=19$m=65536,t=4,p=1$cGY5bm5WcWtvbEZhWHgxMw$AjRIxzPEBulTJM2cWYCe/3coFSVmvMOHHH4e/ZmYAJo'),
(6,'Admin','99999','400000','3/5','43254314','','','','','$argon2i$v=19$m=65536,t=4,p=1$dXYwN09PVjRxM0ZTZkE5Rw$Q6zkHory1225YXLgYO/15rs/nTh5A1TatZoKlBJffFY');

--
-- Table structure for table `secret`
--

DROP TABLE IF EXISTS `secret`;

CREATE TABLE `secret` (
 `message` varchar(300)
);


INSERT INTO `secret` VALUES ('Ceci est une message secret');


CREATE USER selectapp@localhost identified by 'selectapp'; 
GRANT SELECT ON Users.credential to selectapp@localhost; 

CREATE USER updateapp@localhost identified by 'updateapp'; 
GRANT SELECT(ID), UPDATE(PhoneNumber, Address, Email, NickName, Password)
  ON Users.credential to updateapp@localhost; 

FLUSH PRIVILEGES;